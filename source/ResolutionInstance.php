<?php

namespace SlightlyInteractive\DI;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class ResolutionInstance implements ContainerInterface
{
    /** @var EntryMap */
    private $entries;

    /** @var bool */
    private $factoryMode;

    /** @var string[] */
    private $stackItems = [];

    /**
     * @param EntryMap $entries
     * @param bool $factoryMode
     */
    public function __construct(EntryMap $entries, bool $factoryMode)
    {
        $this->entries = $entries;
        $this->factoryMode = $factoryMode;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        if (isset($this->stackItems[$id])) {
            throw new ContainerException("Recursive dependency loop found at class {$id}");
        }

        $this->stackItems[$id] = true;

        if (!isset($this->entries[$id])) {
            $this->entries[$id] = new Entry($id, $this->factoryMode);
        }

        $entry = $this->entries[$id]->resolve($this);

        unset($this->stackItems[$id]);

        return $entry;
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id)
    {
        try {
            $this->get($id);
        } catch (NotFoundExceptionInterface $exception) {
            return false;
        } catch (\Exception $exception) {
        }

        return true;
    }
}