<?php

namespace SlightlyInteractive\DI;

interface EntryInterface
{
    /**
     * Set the argument for a given parameter, by name, for parameters that
     * cannot be autowired.
     *
     * @param string $key
     * @param mixed $value
     * @return EntryInterface
     */
    public function argument(string $key, $value): EntryInterface;

    /**
     * Set a callback that returns an instance of the item.
     * The callable receives the container as a parameter.
     *
     * @param callable $callable
     * @return EntryInterface
     */
    public function callback(callable $callable): EntryInterface;

    /**
     * Sets another DI key that returns the required instance.
     * This can be used to specify the concrete implementation to use for an
     * interface.
     *
     * @param string $instance
     * @return EntryInterface
     */
    public function map(string $instance): EntryInterface;

    /**
     * Specifies that a new instance will be created each time this item is
     * requested, rather than using the default configured for the container.
     *
     * @return EntryInterface
     */
    public function factory(): EntryInterface;

    /**
     * Specifies that a single instance will be re-used and returned whenever
     * this item is requested, rather than using the default configured for
     * the container.
     *
     * @return EntryInterface
     */
    public function singleton(): EntryInterface;
}