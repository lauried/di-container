<?php

namespace SlightlyInteractive\DI;

use Psr\Container\ContainerInterface;

class Entry implements EntryInterface
{
    /** @var string */
    private $id;

    /** @var bool */
    private $factoryMode;

    /** @var callable|null */
    private $callback = null;

    /** @var string */
    private $map = null;

    /** @var array */
    private $arguments = [];

    /** @var mixed */
    private $instance = null;

    /**
     * @param string $id
     * @param bool $factoryMode
     */
    public function __construct(string $id, bool $factoryMode)
    {
        $this->id = $id;
        $this->factoryMode = $factoryMode;
    }

    /**
     * Set the value for a named parameter.
     * If the parameter requires a class, then the value must be the name of
     * a class that will produce the desired
     *
     * @param string $key
     * @param mixed $value
     * @return EntryInterface
     */
    public function argument(string $key, $value): EntryInterface
    {
        $this->callback = null;
        $this->map = null;
        $this->arguments[$key] = $value;
        return $this;
    }

    /**
     * Set a callback that returns an instance of the item.
     * The callable receives a ContainerInterface as a parameter.
     *
     * @param callable $callable
     * @return EntryInterface
     */
    public function callback(callable $callable): EntryInterface
    {
        $this->arguments = [];
        $this->map = null;
        $this->callback = $callable;
        return $this;
    }

    /**
     * Sets another DI key that returns the required instance.
     * This can be used to specify the concrete implementation to use for an
     * interface.
     *
     * @param string $instance
     * @return EntryInterface
     */
    public function map(string $instance): EntryInterface
    {
        $this->arguments = [];
        $this->callback = null;
        $this->map = $instance;
        return $this;
    }

    /**
     * Specifies that a new instance will be created each time this item is
     * requested, rather than using the default configured for the container.
     *
     * @return EntryInterface
     */
    public function factory(): EntryInterface
    {
        $this->factoryMode = true;
        return $this;
    }

    /**
     * Specifies that a single instance will be re-used and returned whenever
     * this item is requested, rather than using the default configured for
     * the container.
     *
     * @return EntryInterface
     */
    public function singleton(): EntryInterface
    {
        $this->factoryMode = false;
        return $this;
    }

    /**
     * @param ContainerInterface $container
     * @return mixed
     * @throws NotFoundException if we can't find an instance of this specific class.
     * @throws ContainerException if anything further down the chain fails.
     */
    public function resolve(ContainerInterface $container)
    {
        if ($this->instance) {
            return $this->instance;
        }

        $instance = null;
        if ($this->callback !== null) {
            $instance = $this->resolveCallback($container);
        } elseif ($this->map !== null) {
            $instance = $this->resolveMap($container);
        } else {
            $instance = $this->resolveAutowire($container);
        }

        if (!$this->factoryMode) {
            $this->instance = $instance;
        }

        return $instance;
    }

    /**
     * @param ContainerInterface $container
     * @return mixed
     */
    private function resolveCallback(ContainerInterface $container)
    {
        try {
            $callback = $this->callback;
            return $callback($container);
        } catch (NotFoundException $exception) {
            throw new ContainerException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param ContainerInterface $container
     * @return mixed
     */
    private function resolveMap(ContainerInterface $container)
    {
        try {
            return $container->get($this->map);
        } catch (NotFoundException $exception) {
            throw new ContainerException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @param ContainerInterface $container
     * @return object
     */
    private function resolveAutowire(ContainerInterface $container)
    {
        $class = $this->id;

        if (!class_exists($class)) {
            spl_autoload_call($class);
        }

        if (!class_exists($class)) {
            throw new NotFoundException("Could not find class {$class}");
        }

        try {
            $reflectionClass = new \ReflectionClass($class);
        } catch (\ReflectionException $exception) {
            throw new ContainerException("Reflection error trying to instantiate class {$class}");
        }

        $constructor = $reflectionClass->getConstructor();

        if ($constructor instanceof \ReflectionMethod) {
            $arguments = [];
            foreach ($constructor->getParameters() as $index => $parameter) {
                $name = $parameter->getName();
                if (isset ($this->arguments[$name])) {
                    if ($parameter->getClass() === null) {
                        $arguments[] = $this->arguments[$name];
                    } else {
                        $arguments[] = $container->get($this->arguments[$name]);
                    }
                    continue;
                }

                $parameterClass = $parameter->getClass();
                if ($parameterClass === null) {
                    throw new ContainerException("Could not deduce parameter type when instantiating class {$class}");
                }

                $arguments[] = $container->get((string)($parameter->getClass()->getName()));
            }
            return $reflectionClass->newInstanceArgs($arguments);
        } else {
            return new $class();
        }
    }
}
