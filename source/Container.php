<?php

namespace SlightlyInteractive\DI;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class Container implements ContainerInterface
{
    /** @var string */
    const INSTANTIATE_FACTORY = 'factory';

    /** @var string */
    const INSTANTIATE_SINGLETON = 'singleton';

    /** @var bool */
    private $factoryMode;

    /** @var EntryMap */
    private $entries;

    /**
     * @param string $instantiateMode
     */
    public function __construct($instantiateMode = self::INSTANTIATE_SINGLETON)
    {
        $this->entries = new EntryMap();

        switch ($instantiateMode) {
            case self::INSTANTIATE_FACTORY:
                $this->factoryMode = true;
                break;
            case self::INSTANTIATE_SINGLETON:
                $this->factoryMode = false;
                break;
            default:
                throw new \InvalidArgumentException('Invalid instantiation mode');
        }
    }

    /**
     * Returns an object which can be used to configure how the given item is
     * instantiated.
     *
     * @param string $id Identifier of the entry to configure.
     * @return EntryInterface
     */
    public function set(string $id): EntryInterface
    {
        if (!isset($this->entries[$id])) {
            $this->entries[$id] = new Entry($id, $this->factoryMode);
        }

        return $this->entries[$id];
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get($id)
    {
        $resolutionInstance = new ResolutionInstance($this->entries, $this->factoryMode);
        return $resolutionInstance->get($id);
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id)
    {
        try {
            $this->get($id);
        } catch (NotFoundExceptionInterface $exception) {
            return false;
        } catch (\Exception $exception) {
        }

        return true;
    }
}
