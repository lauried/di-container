<?php

namespace SlightlyInteractive\DI;

use Psr\Container\ContainerExceptionInterface;

class ContainerException extends \LogicException implements ContainerExceptionInterface
{
}
