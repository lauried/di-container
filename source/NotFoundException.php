<?php

namespace SlightlyInteractive\DI;

use Psr\Container\NotFoundExceptionInterface;

class NotFoundException extends \LogicException implements NotFoundExceptionInterface
{
}
