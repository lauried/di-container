<?php

namespace SlightlyInteractive\DI\Test\TestClasses;

class InterfaceDependent
{
    /** @var DiamondBottomInterface */
    private $dependency;

    /**
     * @param DiamondBottomInterface $dependency
     */
    public function __construct(DiamondBottomInterface $dependency)
    {
        $this->dependency = $dependency;
    }

    /**
     * @return DiamondBottomInterface
     */
    public function getDependency()
    {
        return $this->dependency;
    }
}