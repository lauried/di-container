<?php

namespace SlightlyInteractive\DI\Test\TestClasses;

interface DiamondBottomInterface
{
    /**
     * @return DiamondLeft
     */
    public function getLeft(): DiamondLeft;

    /**
     * @return DiamondRight
     */
    public function getRight(): DiamondRight;
}