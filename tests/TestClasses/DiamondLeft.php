<?php

namespace SlightlyInteractive\DI\Test\TestClasses;

class DiamondLeft
{
	/** @var DiamondTop */
	private $top;

    /**
     * @param DiamondTop $top
     */
	public function __construct(DiamondTop $top)
	{
		$this->top = $top;
	}

    /**
     * @return DiamondTop
     */
	public function getTop(): DiamondTop
    {
        return $this->top;
    }
}

