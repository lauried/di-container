<?php

namespace SlightlyInteractive\DI\Test\TestClasses;

class PartialAutoWireable
{
	/** @var DiamondLeft */
	private $left;

	/** @var string */
	private $name;

	/** @var DiamondRight */
	private $right;

	/**
	 * @param DiamondLeft $left
	 * @param string $name
	 * @param DiamondRight $right
	 */
	public function __construct(DiamondLeft $left, string $name, DiamondRight $right)
	{
		$this->left = $left;
		$this->name = $name;
		$this->right = $right;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

    /**
     * @return DiamondLeft
     */
	public function getLeft(): DiamondLeft
    {
        return $this->left;
    }

    /**
     * @return DiamondRight
     */
    public function getRight(): DiamondRight
    {
        return $this->right;
    }
}
