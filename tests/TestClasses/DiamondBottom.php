<?php

namespace SlightlyInteractive\DI\Test\TestClasses;

class DiamondBottom implements DiamondBottomInterface
{
	/** @var DiamondLeft */
	private $left;

	/** @var DiamondRight */
	private $right;

	/**
	 * @param DiamondLeft $left
	 * @param DiamondRight $right
	 */
	public function __construct(DiamondLeft $left, DiamondRight $right)
	{
		$this->left = $left;
		$this->right = $right;
	}

    /**
     * @return DiamondLeft
     */
	public function getLeft(): DiamondLeft
    {
        return $this->left;
    }

    /**
     * @return DiamondRight
     */
    public function getRight(): DiamondRight
    {
        return $this->right;
    }
}

