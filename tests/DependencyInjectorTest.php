<?php

namespace SlightlyInteractive\DI\Test;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use SlightlyInteractive\DI\Container;
use SlightlyInteractive\DI\ContainerException;
use SlightlyInteractive\DI\NotFoundException;
use SlightlyInteractive\DI\Test\TestClasses\DiamondBottom;
use SlightlyInteractive\DI\Test\TestClasses\DiamondBottomInterface;
use SlightlyInteractive\DI\Test\TestClasses\DiamondLeft;
use SlightlyInteractive\DI\Test\TestClasses\DiamondRight;
use SlightlyInteractive\DI\Test\TestClasses\DiamondTop;
use SlightlyInteractive\DI\Test\TestClasses\InterfaceDependent;
use SlightlyInteractive\DI\Test\TestClasses\PartialAutoWireable;

class ContainerTest extends TestCase
{
    /**
     * Check that diamond dependency produces a single top element in
     * singleton mode.
     */
    public function testDiamondSingleton()
    {
        $container = new Container(Container::INSTANTIATE_SINGLETON);

        $bottom = $container->get(DiamondBottom::class);
        $this->assertSame($bottom->getLeft()->getTop(), $bottom->getRight()->getTop());
    }

    /**
     * Check that diamond dependency produces different top instances in
     * factory mode.
     */
    public function testDiamondFactory()
    {
        $container = new Container(Container::INSTANTIATE_FACTORY);

        $bottom = $container->get(DiamondBottom::class);
        $this->assertNotSame($bottom->getLeft()->getTop(), $bottom->getRight()->getTop());
    }

    /**
     * Check that setting singleton on a specific entry takes precedence over
     * default factory behaviour.
     */
    public function testDiamondSingletonOverride()
    {
        $container = new Container(Container::INSTANTIATE_FACTORY);
        $container->set(DiamondTop::class)->singleton();

        $bottom = $container->get(DiamondBottom::class);
        $this->assertSame($bottom->getLeft()->getTop(), $bottom->getRight()->getTop());

        $bottom2 = $container->get(DiamondBottom::class);
        $this->assertNotSame($bottom, $bottom2);
    }

    /**
     * Check that setting factory on a specific entry takes precedence over
     * default singleton behaviour.
     */
    public function testDiamondFactoryOverride()
    {
        $container = new Container(Container::INSTANTIATE_SINGLETON);
        $container->set(DiamondTop::class)->factory();

        $bottom = $container->get(DiamondBottom::class);
        $this->assertNotSame($bottom->getLeft()->getTop(), $bottom->getRight()->getTop());

        $bottom2 = $container->get(DiamondBottom::class);
        $this->assertSame($bottom, $bottom2);
    }

    /**
     * Test setting named parameters.
     */
    public function testArguments()
    {
        $name = 'Hello World :)';
        $container = new Container();
        $container->set(PartialAutoWireable::class)
            ->argument('name', $name);

        $partial = $container->get(PartialAutoWireable::class);
        $this->assertEquals($name, $partial->getName());
    }

    /**
     * Test setting a callback.
     */
    public function testCallback()
    {
        $name = 'Hello World :D';
        $container = new Container();
        $container->set(PartialAutoWireable::class)
            ->callback(function (ContainerInterface $container) use ($name) {
                return new PartialAutoWireable(
                    $container->get(DiamondLeft::class),
                    $name,
                    $container->get(DiamondRight::class)
                );
            });

        $partial = $container->get(PartialAutoWireable::class);
        $this->assertEquals($name, $partial->getName());
    }

    /**
     * Test setting a concrete instance for an interface.
     */
    public function testInstance()
    {
        $container = new Container();
        $container->set(DiamondBottomInterface::class)
            ->map(DiamondBottom::class);

        $bottom = $container->get(DiamondBottomInterface::class);
        $this->assertInstanceOf(DiamondBottom::class, $bottom);
    }

    /**
     * Ensure that we can use any ID we like if we supply enough information
     * to instantiate the class.
     */
    public function testNamedInstance()
    {
        $container = new Container();
        $container->set('MyInstance1')
            ->callback(function (ContainerInterface $container) {
                return $container->get(DiamondBottom::class);
            });
        $container->set('MyInstance2')
            ->map(DiamondBottom::class);

        $this->assertInstanceOf(DiamondBottom::class, $container->get('MyInstance1'));
        $this->assertInstanceOf(DiamondBottom::class, $container->get('MyInstance2'));
    }

    /**
     * Ensure that a not found exception is thrown when the given class cannot
     * be found.
     */
    public function testNotFoundException()
    {
        $this->expectException(NotFoundException::class);

        $container = new Container();
        $container->get('ThisClassDoesntExist');
    }

    /**
     * Ensure that a container exception is thrown when there's a problem
     * resolving an existing key.
     */
    public function testContainerException()
    {
        $this->expectException(ContainerException::class);

        $container = new Container();
        $container->set(DiamondBottomInterface::class)
            ->map('ThisClassDoesntExist');
        $container->get(DiamondBottomInterface::class);
    }

    /**
     * Ensure that a container exception is thrown when a class is requested
     * that has a circular dependency.
     */
    public function testCircularDependency()
    {
        $this->expectException(ContainerException::class);

        $container = new Container();
        $container->set(DiamondBottomInterface::class)
            ->map(DiamondBottomInterface::class);
        $container->get(DiamondBottomInterface::class);
    }

    /**
     * Ensure that if an object is expected and Item::argument is given a class
     * name, it will instantiate that object.
     */
    public function testClassArgument()
    {
        $container = new Container();
        $container->set(InterfaceDependent::class)
            ->argument('dependency', DiamondBottom::class);
        $result = $container->get(InterfaceDependent::class);
        $this->assertInstanceOf(InterfaceDependent::class, $result);
    }
}
